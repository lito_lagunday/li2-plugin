<?php
/**
 * Templater Class Doc Comment
 *
 * @category Class
 * @package  Li2
 * @author   litolagunday@gmail.com
 */

namespace Li2\Core;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\Extension\StringLoaderExtension;
use Limenius\ReactRenderer\Twig\ReactRenderExtension;
use Limenius\ReactRenderer\Renderer\PhpExecJsReactRenderer;

class TemplateEngine {

	private static $twig;
	private const SSR_MODE = 'both';
	use \Li2\Core\Utility\SingletonTrait;

	public function setup($template_dir, $js_bundle, $context){

		$file_loader = new FilesystemLoader($template_dir);
		$twig = new Environment($file_loader);
		$js_renderer = new PhpExecJsReactRenderer($js_bundle, false, $context);
		$react_ext = new ReactRenderExtension($js_renderer, $context, self::SSR_MODE);
		$string_loader_ext = new StringLoaderExtension();

		$twig->addExtension($react_ext);
		$twig->addExtension($string_loader_ext);

		self::$twig = $twig;
	}

	public function render($template, $data){
		return self::$twig->render($template, $data);
	}

	public  function add_filter($filter){
		self::$twig->addFilter($filter);
	}
}

