<?php
/**
 * Context provider for SSR
 *
 * @category Class
 * @package  Li2
 * @author   litolagunday@gmail.com
 */

namespace Li2\Core;

class WPContext  implements \Limenius\ReactRenderer\Context\ContextProviderInterface{

	public function getContext($serverSide){
		global $wp;
		$requestStack = $wp->request;
		return [
			'serverSide' => $serverSide,
			'href' => '',//$request->getSchemeAndHttpHost().$request->getRequestUri(),
			'location' => '',
			'scheme' => '',//$request->getScheme(),
			'host' => '',//$request->getHost(),
			'port' => '',//$request->getPort(),
			'base' => '',//$request->getBaseUrl(),
			'pathname' => '',//$request->getPathInfo(),
			'search' => '',//$request->getQueryString(),
			// 'viewer' => wp_get_current_user(),
			// 'root_url' =>  get_bloginfo( 'url' ),
			'request' =>  ! empty( $requestStack) ? $requestStack : null
		];
	}
}
