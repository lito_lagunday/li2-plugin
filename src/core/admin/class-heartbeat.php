<?php
/**
 * Templater Class Doc Comment
 *
 * @category Class
 * @package  Li2
 * @author   litolagunday@gmail.com
 */

namespace Li2\Core\Admin;


final class Heartbeat {

	use \Li2\Core\Utility\SingletonTrait;
	private static $source_id;
	private static $response_id;
	private static $filter_id;

	public  function setup($conf){
		self::$source_id = $conf['source_id'];
		self::$response_id = $conf['response_id'];
		self::$filter_id = $conf['filter_id'];
		add_filter( 'heartbeat_received', array(self::$instance, 'handle_heartbeat'), 10, 2 );
	}

	public function handle_heartbeat( array $response, array $data ) {

		if ( empty( $data[self::$source_id] ) ) {
			return $response;
		}

		$received_data = $data[	self::$source_id ];
		$response[self::$response_id] = sha1( $received_data );

		return apply_filters(self::$filter_id, $response);
	}

}

