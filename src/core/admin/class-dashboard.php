<?php
namespace Li2\Core\Admin;


final class Dashboard {
	use \Li2\Core\Utility\SingletonTrait;

	private static $config = [];

	public function setup($config){
		self::$config = $config;
		add_action( 'admin_menu', array (self::$instance, 'setup_dashboard_menu'),0);
		add_action( 'admin_init', array (self::$instance, 'register_settings'));
		add_action( 'admin_enqueue_scripts', array (self::$instance, 'register_screen_assets'));
	}

	public function setup_dashboard_menu() {
		$access_type = 'manage_options';
		$callback = array( $this, 'render_screen' );
		add_options_page(	self::$config['name'], 	self::$config['name'], $access_type , self::$config['slug'], $callback);
	}

	public function sanitize( $options ) {
		return $options;
	}

	public function register_settings() {
		$options = array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize' ), 'default' => '',);
		register_setting(self::$config['slug'], self::$config['prefix'] . 'settings', $options);
	}

	public function register_screen_assets(){
		wp_enqueue_script(self::$config['slug'] . '-scripts', self::$config['screen_asset_js'] , '', '', false);
		wp_enqueue_style(self::$config['slug'] . '-styles', self::$config['screen_asset_css'], '', '', false);
	}

	public function render_screen() {
		$default_template = '<p>Hook into <i>' . self::$config['prefix'] . 'admin_screen</i> filter and pass a dashboard template. </p>';
		echo apply_filters( self::$config['prefix']. 'admin_screen', $default_template );
	}
}


