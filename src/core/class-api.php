<?php
namespace  Li2\Core;

final class API {
	private static $instance = null;
	private static $config = [];

	public static function instance(){
		if ( null === self::$instance ){
		}
		return self::$instance;
	}


	public function init() {
		parent::init();
		add_action( 'init', array( $this, 'add_endpoint' ), 0 );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );
		add_action( 'parse_request', array( $this, 'handle_api_requests' ), 0 );
		add_action( 'rest_api_init', array( $this, 'register_wp_admin_settings' ) );
	}

	public function handle_api_requests() {
		global $wp;

		if ( ! empty( $_GET['wc-api'] ) ) { // WPCS: input var okay, CSRF ok.
			$wp->query_vars['wc-api'] = sanitize_key( wp_unslash( $_GET['wc-api'] ) ); // WPCS: input var okay, CSRF ok.
		}

		// wc-api endpoint requests.
		if ( ! empty( $wp->query_vars['wc-api'] ) ) {

				// Buffer, we won't want any output here.
				ob_start();

				// No cache headers.
				wc_nocache_headers();

				// Clean the API request.
				$api_request = strtolower( wc_clean( $wp->query_vars['wc-api'] ) );

				// Make sure gateways are available for request.
				WC()->payment_gateways();

				// Trigger generic action before request hook.
				do_action( 'woocommerce_api_request', $api_request );

				// Is there actually something hooked into this API request? If not trigger 400 - Bad request.
				status_header( has_action( 'woocommerce_api_' . $api_request ) ? 200 : 400 );

				// Trigger an action which plugins can hook into to fulfill the request.
				do_action( 'woocommerce_api_' . $api_request );

				// Done, clear buffer and exit.
				ob_end_clean();
				die( '-1' );
			}
		}


}


