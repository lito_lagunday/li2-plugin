<?php

 $slug = Li2\Dashboard\Admin::$plugin_slug;
 $settings = get_option(  $slug . '_' . 'settings' );
 $name = $settings['name'] ??  '';

?>

<div id="<?php echo $slug?>" class="plugin_dashboard">

	<form method="POST" action="options.php">
		<p>
			<input type="text" name="<?php echo $slug ?>_settings[name]" size="50" value="<?php echo esc_attr($name); ?>">
		</p>
		<?php
			settings_fields($slug);
			submit_button();
		?>
	</form>

</div>

