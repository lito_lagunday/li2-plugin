<?php
/**
 * Pluginname Class Doc Comment
 *
 * @category Class
 * @package  Li2
 * @author   litolagunday@gmail.com
 */

namespace Li2;

final	class Plugin{
	use \Li2\Core\Utility\SingletonTrait;
	private static $config;
	private static $heartbeat;
	private static $admin_dashboard;
	private static $template;

	private function __construct() {
		self::$config = \Li2\Core\ConfigLoader::instance();
		self::$heartbeat = \Li2\Core\Admin\Heartbeat::instance();
		self::$admin_dashboard = \Li2\Core\Admin\Dashboard::instance();
		self::$template = \Li2\Core\TemplateEngine::instance();
	}

	public function init(){
		$this->setup_config();
		$this->setup_WPHeartbeat();
		$this->setup_admin();
		$this->setup_template();
		$this->wireup_hooks();
	}

	private function setup_config() {
		self::$config->setup([__DIR__.'/config'], 'plugin.yaml');
	}

	private function setup_WPHeartbeat() {
		self::$heartbeat->setup(self::$config->get('heartbeat'));
	}

	private function setup_template() {
		$context = new \Li2\Core\WPContext();
		self::$template->setup( __DIR__ . '/templates', __DIR__.'/../assets/admin/dist/server.bundle.js', $context);
		self::$template->add_filter(new \Twig\TwigFilter('html_entity_decode','html_entity_decode'));
	}

	private function setup_admin() {
		self::$admin_dashboard->setup(self::$config->get('admin'));
	}

	private function wireup_hooks() {
		$prefix = self::$config->get('admin')['prefix'];
		add_filter($prefix . 'admin_screen', array ($this, 'admin_screen_filter'), 10, 1 );
	}

	public function admin_screen_filter() {
		$mock_data = ['val1' => 'both', 'val2' => Li2_PLUGIN_URL, 'js_runtime' => Li2_SSR_RUNTIME];
		return self::$template->render('ssr-test.html.twig', $mock_data);
	}

	// private function is_request( $type ) {
	// 	switch ( $type ) {
	// 		case 'admin':
	// 			return is_admin();
	// 		case 'ajax':
	// 			return defined( 'DOING_AJAX' );
	// 		case 'cron':
	// 			return defined( 'DOING_CRON' );
	// 		case 'frontend':
	// 			return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' ) && ! $this->is_rest_api_request();
	// 	}
	// }

}
