<?php
/**
 * Google Spreadsheet Class Doc Comment
 *
 * @category Class
 * @package  Li2
 * @author   litolagunday@gmail.com
 */

namespace Li2\Packages;


if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'Li2\Packages\Spreadsheet' ) ) {

	class Spreadsheet {

		private static $instance;
		private $vars = [];

		public function set_variable($name, $var)
    {
        $this->vars[$name] = $var;
    }

    public function get_html($template)
    {
        foreach($this->vars as $name => $value) {
            $template = str_replace('{' . $name . '}', $value, $template);
        }

        return $template;
    }

		private function __construct() {
		}

		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public static function public_static_function( $content ) {
			return $content;
		}
	}
}
