# Li2-plugin #
###### Created by litolagunday@gmail.com  ######
This is a boilerplate for ReactJS(with SSR on PHP) based WP plugins. It is an implementation of ReactRenderer(https://github.com/Limenius/ReactRenderer).

### Why tho? ###
- Makes Googlebot happy, crawl and indexing is faster content.
- Content can be prioritized/loaded regardless of device's JS capability.
- Smaller possibility of unrendered content(event injected) due to JS code.
- Less JS payload and content calls, better page speed.

### Why not? ###
Certain objects like - document, window, console, may not be avaialable at SSR.

You can configure the entire JS code to run client side only.
Or you can keep the app partially isomorphic by evaluating the components context. The sample code uses dynamic imports through loadable-components(https://loadable-components.com/).

### Server requirements ###
- Requires at least: 7.4
- Tested up to: 7.4
- Requires PHP: 7.4
- NodeJS (for optional SSR)

### Libraries ###
- Symfony dependency injection
- Twig for template engine
- limenius/react-renderer for SSR of ReactJS

### Dev stack ###
- Frontend: NPM (workspaces setup), ES6, Typescript, Eslint, SASS
- Backend: PHP7.4, PHPUnit (WP Core), PHP codesniffer (WP Standards)

Todo:
- [ ] Application level dependency configuration
	- [ ] Dataproviders (WPDB, Firebase)
	- [ ] API components (GraphQL, REST)
	- [ ] Authentication/Authorization components
- [ ] Twig extension for wordpress functions
- [x] Component level SSR
- [x] Wordpress context provider for client side react
- [x] WP Hearbeat API
- [x] Decoupled Frontend(Admin, Public)


## Front End Development ##

## Backend End Development ##
