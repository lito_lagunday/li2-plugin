<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @wordpress-plugin
 * Plugin Name:     Li2 Pluginname
 * Plugin URI:      http://codes.lagunday.com/li2-plugin
 * Description:     Boilerplate for custom GraphQL+ReactJS based plugins
 * Author:          litolagunday@gmail.com
 * Author URI:      http://codes.lagunday.com
 * Version:         0.1.0
 *
 * @package         Li2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
	require __DIR__ . '/vendor/autoload.php';
}

if ( ! defined( 'Li2_PLUGIN_URL' ) ) {
	define( 'Li2_PLUGIN_URL', plugin_dir_url(  __DIR__  . '/bootstrap.php' ) );
}

register_deactivation_hook( __FILE__, 'li2_plugin_demo_deactivation' );

function li2_plugin_demo_deactivation() {
	// Delete Data
	// Delete Registered settings
}


function php_requirements_error() {
	$class = 'notice notice-error';
	$message = Li2_PLUGIN_NAME . ' requires PHP 7.4 or above.' ;

	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
}

//Check requirements
$php_version = phpversion();

$requirements_failed = false;

if (version_compare($php_version, "7.4") === -1) {
    add_action( 'admin_notices', 'php_requirements_error' );
    $requirements_failed = true;
}


if ($requirements_failed) {
	return;
}



if ( ! defined( 'Li2_SSR_RUNTIME' ) ) {
	$phpExecJs = new \Nacmartin\PhpExecJs\PhpExecJs();
	define( 'Li2_SSR_RUNTIME', $phpExecJs->getRuntimeName());
}

//Create Plugin Instance
$plugin = \Li2\Plugin::instance();
$plugin->init();

