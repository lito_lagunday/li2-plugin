<?php
define( 'WP_TESTS_CONFIG_FILE_PATH', __DIR__.  "/wp-tests-config.php");

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__. "/wptest/includes/functions.php";
require __DIR__.  "/wptest/includes/bootstrap.php";

ini_set('error_reporting', E_ERROR );
