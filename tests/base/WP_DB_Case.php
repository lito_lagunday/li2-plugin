<?php
namespace  Li2\Tests\Base;
use WP_UnitTestCase;
use WpdbExposedMethodsForTesting;

class WP_DB_Case extends WP_UnitTestCase{
	protected static $_wpdb;

	public function set_up() {
		parent::set_up();
		$this->_queries = array();
		add_filter( 'query', array( $this, 'query_filter' ) );
		self::$_wpdb->last_error     = null;
		$GLOBALS['wpdb']->last_error = null;
	}

	public function tear_down() {
		parent::tear_down();
	}

	public static function set_up_before_class() {
		parent::set_up_before_class();
		self::$_wpdb = new WpdbExposedMethodsForTesting();
	}

	public function query_filter( $sql ) {
		$this->_queries[] = $sql;
		return $sql;
	}

	protected function printToConsole($var) {
		fwrite(STDERR, print_r($var , TRUE));
	}

}
