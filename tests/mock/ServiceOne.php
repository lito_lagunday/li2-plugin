<?php
namespace Li2\Tests\Mock;


class ServiceOne {
	private static $instance;
	private function __construct() {
	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}

