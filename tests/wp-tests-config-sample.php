<?php


/**
** Absolute path of the wordpress installation.
** path/to/bedrock/web/wp/  for Brdrock and
** installation folder for regular Wordpress
**/
define( 'ABSPATH', '' );

// BE REMINDED!!! THIS SUITE WILL ALWAYS DELETE DB CONTENT.
// USE A TEMPORARY DATABASE
define( 'DB_NAME', '' );
define( 'DB_USER', '' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', '' );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// Change these to different unique phrases! You can generate these using
// the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.

define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );

$table_prefix = 'wp_';

define( 'WP_TESTS_DOMAIN', 'example.org' );
define( 'WP_TESTS_EMAIL', 'admin@example.org' );
define( 'WP_TESTS_TITLE', 'Test Blog' );
define( 'WP_PHP_BINARY', 'php' );
define( 'WPLANG', '' );
define( 'WP_DEFAULT_THEME', 'default' );
define( 'WP_DEBUG', true );
