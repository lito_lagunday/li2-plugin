<?php

use Li2\Tests\Base\WP_DB_Case;
use Li2\Pluginname;


final class ContainerTest extends WP_DB_Case{
	/**
	 * @group test_group1
	 */
	public function test_db_connection() {

		global $wpdb;
		$var2 = $wpdb->get_var( "SELECT option_name FROM $wpdb->options LIMIT 1" );

		$name = Pluginname::get_name();

		echo "\n***Just peeking***\n\n";
		print_r($name);
		print_r($var2);
		echo "\n\n******************\n";

		$wpdb->check_connection();
		$var = $wpdb->get_var( "SELECT ID FROM $wpdb->users LIMIT 1" );
		$this->assertGreaterThan( 0, $var );

		$wpdb->close();
	}

}
