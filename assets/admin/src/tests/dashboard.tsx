import React from 'react';
import loadable from '@loadable/component'

const today = new Date();

interface IComponentProps {
	railsContext:any
}

interface IComponentState {
}

class DashboardBase extends React.Component<IComponentProps, IComponentState>{

	state = {
		heartbeat: 'init',
	};

	constructor(props) {
		super(props);
		this.heartbeatSendHandler = this.heartbeatSendHandler.bind(this);
		this.heartbeatTickHandler = this.heartbeatTickHandler.bind(this);
	}

	heartbeatSendHandler(event, data ){
		data.li2_plugin_demo_client = today.getSeconds();
	}

	heartbeatTickHandler(event, data ){
		this.setState({heartbeat : data.li2_plugin_demo_hashed})
	}

	componentDidMount() {
		if(window){
			if(window.hasOwnProperty('jQuery')){
				const jQuery = window['jQuery'];
				jQuery(document).on( 'heartbeat-send', this.heartbeatSendHandler);
				jQuery(document).on( 'heartbeat-tick', this.heartbeatTickHandler);
			}
		}
	}

  render (){

		const serverSide:string = this.props.railsContext.serverSide;
		let container = (() => <div>This is rendered server side. <a className={'button'}>Regular Link Button</a></div>);

		if(!serverSide){
			const Button = loadable(() => import('@mui/material/Button'), { ssr: false });
			container = (() => <div>This is hydrated client side. <Button>Material UI Button</Button></div>)
		}

    return (
			<div  className='dashboard app-a' >
				<p>WP Heartbeat: {this.state.heartbeat}</p>

				{container()}
			</div>
		);
  }
}

const AdminDashboard = (props, railsContext) => (() => <DashboardBase {...{...props, railsContext}}/>)

export default AdminDashboard;
