import React from 'react';
import loadable from '@loadable/component'

interface IComponentProps {
	railsContext:any
}

interface IComponentState {
}

class SampleComponent extends React.Component<IComponentProps, IComponentState>{

	constructor(props) {
		super(props);
	}

  render (){

		let context_string = JSON.stringify(this.props.railsContext, null, 4);
		const serverSide:string = this.props.railsContext.serverSide;
		let container = (() =>  <div>This is rendered server side. <a className={'button'}>Regular Link Button</a></div>);

		if(!serverSide){
			const Button = loadable(() => import('@mui/material/Button'), { ssr: false });
			container = (() => <div>This is hydrated client side. <Button>Material UI Button</Button></div>)
		}

    return (
				<div className='dashboard app-b'>
					 {container()}
				</div>
		);
  }
}

const SampleComponentWithContext = (props, railsContext) => (() => <SampleComponent {...{...props, railsContext}}/>)

export default SampleComponentWithContext;
