import dashboard from './tests/dashboard';
import samplecomponent from './tests/samplecomponent';
import clientonly from './tests/clientonlycomponent';
import ReactOnRails from 'react-on-rails';

ReactOnRails.register({ClientHydrated : dashboard});
ReactOnRails.register({ClientRendered : clientonly});
ReactOnRails.register({ServerRendered: samplecomponent });
