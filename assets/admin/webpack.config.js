const path = require('path');
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');

module.exports = {
  mode: "development",
  devtool: "inline-source-map",
  entry: {
    server: './src/server_side.ts',
		client: './src/client_side.ts',
    styles: './src/styles/main.scss'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname + '/dist'),
		publicPath: '	/app/plugins/li2-plugin/assets/admin/dist/',
		globalObject: 'this',
		chunkFilename:  'ui-[contenthash].js',
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },{
        test: /\.scss$/,
        exclude: /node_modules/,
        type: 'asset/resource',
        generator: {
          filename: 'css/[name].css'
        },
        use: [
          'sass-loader'
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    modules: ['./node_modules/'],
  },
  plugins: [
    new RemoveEmptyScriptsPlugin(),
  ]
};
