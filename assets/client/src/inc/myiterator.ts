type Result = 'pass' | 'fail'

let result : Result = 'pass';

const myIterator = function iterator(x : number){
  if(x == 300){return}
  const n = x + 1;
  iterator(n);


  if (result === 'pass') {
    console.warn('Passed')
  } else {
    console.warn('Failed')
  }
}

export default myIterator;
