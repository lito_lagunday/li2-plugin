import React from 'react';

type Result = 'pass' | 'fail'
let result : Result = 'pass';

class Home extends React.Component {
  render (){
    return <div>Home {result}</div>;
  }
}

export default Home;
