import Home from './pages/home';
import ReactOnRails from "react-on-rails";


ReactOnRails.register({ Home });




export const start = async () => {
	const homepage = new Home;
  homepage.render();
  console.log('starting');
};

start();
